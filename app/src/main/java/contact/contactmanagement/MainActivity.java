package contact.contactmanagement;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import contact.contactmanagement.DatabaseUtils.Contacts;
import contact.contactmanagement.DatabaseUtils.ContactsDataSource;
import contact.contactmanagement.Utils.ContactDBConstants;
import contact.contactmanagement.Utils.CustomListItemInterface;
import contact.contactmanagement.Utils.DownloadListener;
import contact.contactmanagement.Utils.JSONUtils;

public class MainActivity extends AppCompatActivity implements CustomListItemInterface {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.contact_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAddContactActivity();
            }
        });
        textView = (TextView) findViewById(R.id.show_error);
    }

    @Override
    protected void onResume() {
        super.onResume();
        downloadListData();
    }

    private void downloadListData(){
        if(findViewById(R.id.avloadingIndicatorView) !=null) {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);
        }
        String url = "http://gojek-contacts-app.herokuapp.com/contacts.json";
        downloadUrl(url, new DownloadListener() {
            @Override
            public void onDataDownloaded(ArrayList<String> dataList, HashMap<String, String> dataHashmap, Object dataObject) {

                if (dataObject != null) {
                    ArrayList<Contacts> contactList = (ArrayList) dataObject;
                    if (contactList.size() == 0) {
                        textView.setVisibility(View.VISIBLE);
                    } else {
                        ContactAdapter mAdapter = new ContactAdapter(contactList, MainActivity.this, MainActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Not able to connect to server", Toast.LENGTH_SHORT).show();
                }
                if (findViewById(R.id.avloadingIndicatorView) != null) {
                    findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.add_contact) {
            startAddContactActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startAddContactActivity() {
        Intent i = new Intent(MainActivity.this, AddContactActivity.class);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("TAG","ONACTIVITY");
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                View v = findViewById(R.id.contact_recycler_view);
                Snackbar.make(v, "Succesfully Added", Snackbar.LENGTH_SHORT).show();
                downloadListData();
            }
        }
    }

    public void downloadUrl(final String url, final DownloadListener downloadListener) {

        new AsyncTask<String, Void, ArrayList<Contacts>>() {

            OkHttpClient client = new OkHttpClient();

            @Override
            protected ArrayList<Contacts> doInBackground (String...params){

                Request.Builder builder = new Request.Builder();
                builder.url(url);

                Request request = builder.build();

                Response response = null;
                try {
                    response = client.newCall(request).execute();
                    if (response != null && response.isSuccessful()) {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        ArrayList<HashMap<String, Object>> arrayList = JSONUtils.toList(jsonArray);
                        ContactsDataSource.SaveContactList(arrayList, MainActivity.this);
                        for(HashMap<String,Object> map : arrayList){
                            getContactDataFromServer((String) map.get(ContactDBConstants.CONTACT_URL),MainActivity.this);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                } finally {
                    return ContactsDataSource.getContactListFromDb(MainActivity.this);
                }

            }

            @Override
            protected void onPostExecute (ArrayList < Contacts > contacts) {
                super.onPostExecute(contacts);
                downloadListener.onDataDownloaded(null, null, contacts);
            }

        }.execute();
    }

    private void getContactDataFromServer(final String url, final Context context) {

        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        builder.url(url);
        Request request = builder.build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response != null && response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                ContactsDataSource.SaveContactList(new HashMap<String, Object>(JSONUtils.jsonToMap(jsonObject)), context);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onListItemClickListener(View v, Contacts object) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.contact_layout_container:
                Intent intent = new Intent(MainActivity.this,ContactDetails.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("CONTACT_DETAILS",object);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}
