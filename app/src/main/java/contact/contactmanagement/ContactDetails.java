package contact.contactmanagement;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import contact.contactmanagement.DatabaseUtils.Contacts;
import contact.contactmanagement.DatabaseUtils.ContactsDataSource;
import contact.contactmanagement.Utils.ContactDBConstants;
import contact.contactmanagement.Utils.DownloadListener;
import contact.contactmanagement.Utils.JSONUtils;
import contact.contactmanagement.Utils.TextUtils;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactDetails extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_PHONE_CALL = 1000;
    private static final int MY_PERMISSIONS_REQUEST_SMS = 2000;
    private static final String KEY_CONTACT_DATA = "KEY_CONTACT_DATA";
    TextView name;
    TextView mobile_no;
    TextView email;
    ImageView mobile_img;
    ImageView email_img;
    CircleImageView profile_img;
    ImageView favourite_img;
    private String favouriteValue;
    private Contacts contactData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Contacts contacts;
        if(savedInstanceState != null){
             contacts = (Contacts) savedInstanceState.getSerializable(KEY_CONTACT_DATA);
        }else {
            Bundle bundle = getIntent().getExtras();
            contacts = (Contacts) bundle.getSerializable("CONTACT_DETAILS");
        }
        setContentView(R.layout.contact_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        name = (TextView) findViewById(R.id.name_text_view);
        mobile_no = (TextView) findViewById(R.id.phone_no);
        email = (TextView) findViewById(R.id.email);
        mobile_img = (ImageView) findViewById(R.id.phone_img);
        email_img = (ImageView) findViewById(R.id.email_img);
        profile_img = (CircleImageView) findViewById(R.id.profile_image);
        favourite_img = (ImageView) findViewById(R.id.favourite_img);
        setSupportActionBar(toolbar);
        profile_img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mobile_img.setOnClickListener(this);
        mobile_no.setOnClickListener(this);
        email.setOnClickListener(this);
        email_img.setOnClickListener(this);
        favourite_img.setOnClickListener(this);

        setLoadingIndicator();
        getContactDataFromServer( new DownloadListener() {
            @Override
            public void onDataDownloaded(ArrayList<String> dataList, HashMap<String, String> dataHashmap, Object dataObject) {

                Contacts currentContact = contacts;
                if (dataObject != null && dataObject instanceof Contacts) {
                    currentContact = (Contacts) dataObject;
                }
                Glide.with(ContactDetails.this).load(currentContact.getProfile_url()).placeholder(R.drawable.missing_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(profile_img);
                contactData = currentContact;
                favouriteValue = currentContact.getIsFavourite();
                name.setText(currentContact.getFirstName() + " " + currentContact.getLastName());
                if (!TextUtils.isEmpty(currentContact.getMobileNo())) {
                    mobile_no.setText(currentContact.getMobileNo());
                }
                if (!TextUtils.isEmpty(currentContact.getEmail())) {
                    email.setText(currentContact.getEmail());
                }
                if(favouriteValue.equalsIgnoreCase("1")) {
                    favourite_img.setImageDrawable(getResources().getDrawable(R.drawable.favourite));
                }else {
                    favourite_img.setImageDrawable(getResources().getDrawable(R.drawable.not_favourite));
                }
            }
        }, this, contacts.getContactId());

        hideLoadingIndicator();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(KEY_CONTACT_DATA,contactData);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact_menu_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.send_sms) {
            if (ContextCompat.checkSelfPermission(ContactDetails.this,
                    Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ContactDetails.this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SMS);
            }else {
                sendMessage();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getContactDataFromServer(final DownloadListener downloadListener, final Context context, final long contactId) {
        new AsyncTask<Void, Void, Object>() {

            OkHttpClient client = new OkHttpClient();

            @Override
            protected Object doInBackground(Void... params) {
                        return ContactsDataSource.getContact(context, contactId);
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                downloadListener.onDataDownloaded(null, null, o);
            }
        }.execute();
    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.phone_img:
            case R.id.phone_no:
                if (ContextCompat.checkSelfPermission(ContactDetails.this,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactDetails.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_PHONE_CALL);


                }else {
                   call();
                }
                break;
            case R.id.email:
            case R.id.email_img:
                if(email != null){
                    String emailId = email.getText().toString();
                    if(!TextUtils.isEmpty(emailId)){
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto",emailId, null));
                        startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    }
                }

                break;
            case R.id.favourite_img:
                try {
                    String url = "http://gojek-contacts-app.herokuapp.com/contacts";
                    setLoadingIndicator();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(ContactDBConstants.CONTACT_ID,contactData.getContactId());
                    jsonObject.put(ContactDBConstants.CONTACT_FIRST_NAME, contactData.getFirstName());
                    jsonObject.put(ContactDBConstants.CONTACT_LAST_NAME, contactData.getLastName());
                    jsonObject.put(ContactDBConstants.CONTACT_MOBILE_NUMBER, mobile_no.getText());
                    jsonObject.put(ContactDBConstants.CONTACT_EMAIL_ID, email.getText());
                    jsonObject.put(ContactDBConstants.CONTACT_PROFILE_PIC, contactData.getProfile_url());
                    if(favouriteValue.equalsIgnoreCase("1")) {
                        jsonObject.put(ContactDBConstants.IS_FAVOURITE_CONTACT, "false");
                        favouriteValue = "0";
                        favourite_img.setImageDrawable(getResources().getDrawable(R.drawable.not_favourite));
                    }else {
                        jsonObject.put(ContactDBConstants.IS_FAVOURITE_CONTACT, "true");
                        favouriteValue = "1";
                        favourite_img.setImageDrawable(getResources().getDrawable(R.drawable.favourite));
                    }

                    addContactAsFavourite(jsonObject, url, new DownloadListener() {
                        @Override
                        public void onDataDownloaded(ArrayList<String> dataList, HashMap<String, String> dataHashmap, Object dataObject) {
                            hideLoadingIndicator();
                            if (dataObject instanceof Boolean && (Boolean) dataObject) {
                                Snackbar.make(mobile_no,"Contact Added as Favourite",Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                }catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

    private void addContactAsFavourite(final JSONObject contacts, final String upload_url, final DownloadListener downloadListener) {
        new AsyncTask<JSONObject,Void,Boolean>(){

            @Override
            protected Boolean doInBackground(JSONObject... params) {
                OkHttpClient client = new OkHttpClient();

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                //Log.d("TAG",params[0].toString());
                RequestBody body = RequestBody.create(JSON, params[0].toString());

                Request request = new Request.Builder()
                        .url(upload_url)
                        .post(body)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()) {
                        return true;
                    }
                    else {
                        return false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean o) {
                super.onPostExecute(o);
                if(o == false){
                    showNetworkErrorAlert();
                }
                downloadListener.onDataDownloaded(null,null,o);
            }
        }.execute(contacts);
    }

    private void showNetworkErrorAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Network Error").setMessage("Error Connecting To Server").setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_PHONE_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendMessage();
                }
                return;
            }
        }
    }

    private void call(){
        if(mobile_no != null){
            String phoneNumber = mobile_no.getText().toString();
            if(!TextUtils.isEmpty(phoneNumber)){
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phoneNumber));
                startActivity(callIntent);
            }
        }
    }

    private void sendMessage(){
        if(mobile_no != null){
            String phoneNumber = mobile_no.getText().toString();
            if(!TextUtils.isEmpty(phoneNumber)){
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null)));
            }
        }
    }

    private void setLoadingIndicator() {
        if(findViewById(R.id.avloadingIndicatorView) !=null) {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);
        }
    }

    private void hideLoadingIndicator() {
        if(findViewById(R.id.avloadingIndicatorView) !=null) {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);
        }
    }


}
