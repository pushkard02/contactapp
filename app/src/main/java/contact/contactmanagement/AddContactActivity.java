package contact.contactmanagement;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import contact.contactmanagement.Utils.ContactDBConstants;
import contact.contactmanagement.Utils.DownloadListener;
import contact.contactmanagement.Utils.TextUtils;

public class AddContactActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AddContactActivity.class.getSimpleName();
    private static final String PROFILE_IMAGE_FILE_PATH = "PROFILE_IMAGE_FILE_PATH";
    private static final int MY_PERMISSIONS_REQUEST_READ_WRITE = 1001;
    EditText first_name;
    EditText last_name;
    EditText mobile_number;
    EditText email;
    ImageView profile_pic;
    File finalFile;
    private DialogPlus mDialogPlus;
    private String filePath;
    private CheckBox fav_checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            filePath = savedInstanceState.getString(PROFILE_IMAGE_FILE_PATH);
        }
        setContentView(R.layout.add_contact_form);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button button = (Button) findViewById(R.id.save_button);
        button.setOnClickListener(this);
        first_name = (EditText) findViewById(R.id.first_name_edt);
        last_name = (EditText) findViewById(R.id.last_name_edt);
        mobile_number = (EditText) findViewById(R.id.mobile_number_edt);
        email = (EditText) findViewById(R.id.email_address_edt);
        profile_pic = (ImageView) findViewById(R.id.profile_image);
        profile_pic.setOnClickListener(this);
        fav_checkbox = (CheckBox) findViewById(R.id.fav_checkbox);
        fav_checkbox.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PROFILE_IMAGE_FILE_PATH,filePath);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(final View v) {
        int viewId = v.getId();
        switch (viewId){
            case R.id.profile_image:
                    askForReadWritePermission();
                break;
            case R.id.save_button:
                checkAndSaveData(v);
                break;
        }
    }


    private void checkAndSaveData(final View v){

        String first_name_text = String.valueOf(first_name.getText());
        String last_name_text = String.valueOf(last_name.getText());
        String mobile_no = String.valueOf(mobile_number.getText());
        String email_id = String.valueOf(email.getText());
        String favourite = String.valueOf(fav_checkbox.isChecked());
        boolean proceedToSave = true;
        if(TextUtils.isEmpty(first_name_text) || first_name_text.length() <3){
            first_name.setError("Invalid Name");
            proceedToSave = false;
        }
        if(TextUtils.isEmpty(mobile_no) || (mobile_no.length() <10 || mobile_no.length()>15)){
            mobile_number.setError("Invalid Mobile Number");
            proceedToSave = false;
        }

        if(proceedToSave){
            dataToUpload();
        }

    }

    private void dataToUpload(){
        try {
            String url = "http://gojek-contacts-app.herokuapp.com/contacts";
            String first_name_text = String.valueOf(first_name.getText());
            String last_name_text = String.valueOf(last_name.getText());
            String mobile_no = String.valueOf(mobile_number.getText());
            String email_id = String.valueOf(email.getText());
            String favourite = String.valueOf(fav_checkbox.isChecked());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ContactDBConstants.CONTACT_FIRST_NAME, first_name_text);
            jsonObject.put(ContactDBConstants.CONTACT_LAST_NAME,last_name_text);
            jsonObject.put(ContactDBConstants.CONTACT_MOBILE_NUMBER,mobile_no);
            jsonObject.put(ContactDBConstants.CONTACT_EMAIL_ID,email_id);
            jsonObject.put(ContactDBConstants.CONTACT_PROFILE_PIC,filePath);
            jsonObject.put(ContactDBConstants.IS_FAVOURITE_CONTACT,favourite);
            setLoadingIndicator();
            UploadData(jsonObject, url, new DownloadListener() {
                @Override
                public void onDataDownloaded(ArrayList<String> dataList, HashMap<String, String> dataHashmap, Object dataObject) {
                    hideLoadingIndicator();
                    if (dataObject instanceof Boolean && (Boolean) dataObject) {
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void UploadData(final JSONObject contacts, final String upload_url, final DownloadListener downloadListener) {
        new AsyncTask<JSONObject,Void,Boolean>(){

            @Override
            protected Boolean doInBackground(JSONObject... params) {
                OkHttpClient client = new OkHttpClient();

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                //Log.d("TAG",params[0].toString());
                RequestBody body = RequestBody.create(JSON, params[0].toString());
                /*final MediaType MEDIA_TYPE = MediaType.parse("image/jpeg");
                RequestBody requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addPart(
                                Headers.of("Content-Disposition", "form-data; name=\"title\""),
                                RequestBody.create(null, "Square Logo"))
                        .addPart(
                                Headers.of("Content-Disposition", "form-data; name=\"image\""),
                                RequestBody.create(MEDIA_TYPE, finalFile))
                        .addFormDataPart(ContactDBConstants.CONTACT_FIRST_NAME, contacts.getFirstName())
                        .addFormDataPart(ContactDBConstants.CONTACT_LAST_NAME, contacts.getLastName())
                        .addFormDataPart(ContactDBConstants.CONTACT_MOBILE_NUMBER, contacts.getMobileNo())
                        .addFormDataPart(ContactDBConstants.CONTACT_EMAIL_ID, contacts.getEmail())
                        .build();

                RequestBody requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart(ContactDBConstants.CONTACT_FIRST_NAME, contacts.getFirstName())
                        .addFormDataPart(ContactDBConstants.CONTACT_LAST_NAME, contacts.getLastName())
                        .addFormDataPart(ContactDBConstants.CONTACT_MOBILE_NUMBER, contacts.getMobileNo())
                        .addFormDataPart(ContactDBConstants.CONTACT_EMAIL_ID, contacts.getEmail())
                        .addFormDataPart("file", "profile.jpeg", RequestBody.create(MEDIA_TYPE, finalFile))
                        .build();*/

                Request request = new Request.Builder()
                        .url(upload_url)
                        .post(body)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if(response.isSuccessful()) {
                        return true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean o) {
                super.onPostExecute(o);
                if(o == false){
                    showNetworkErrorAlert();
                }
                downloadListener.onDataDownloaded(null,null,o);
            }
        }.execute(contacts);
    }

    private void showNetworkErrorAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Network Error").setMessage("Error Connecting To Server").setCancelable(true).setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataToUpload();
                    }
                }).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * Get the URI of the selected image from  {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ?  getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = AddContactActivity.this.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new  File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Intent getPickImageChooserIntent() {

// Determine Uri of camera image to  save.
        Uri outputFileUri =  getCaptureImageOutputUri();

        List<Intent> allIntents = new  ArrayList<>();
        PackageManager packageManager =  AddContactActivity.this.getPackageManager();

// collect all camera intents
        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam =  packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new  Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            }
            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new  Intent(galleryIntent);
            intent.setComponent(new  ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list  so pickup the useless one
        Intent mainIntent =  allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if  (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity"))  {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent =  Intent.createChooser(mainIntent, "Select source");

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,  allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200 && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);


            View headerView = AddContactActivity.this.getLayoutInflater().inflate(R.layout.image_cropper_header, null);
            View mainContainer = AddContactActivity.this.getLayoutInflater().inflate(R.layout.image_profile_cropper, null);

            final CropImageView cropImageView = (CropImageView) mainContainer.findViewById(R.id.CropImageView);
            final TextView cancleBtn = (TextView) mainContainer.findViewById(R.id.cancel_crop);
            final TextView submitBtn = (TextView) mainContainer.findViewById(R.id.crop_and_save);

            TextView headerText = (TextView) headerView.findViewById(R.id.header_title);
            ImageView headerCloseBtn = (ImageView) headerView.findViewById(R.id.header_close_btn);

            headerText.setText("Please Crop");
            headerText.setTextColor(getResources().getColor(R.color.colorAccent));
            cropImageView.setFixedAspectRatio(true);
            cropImageView.setAspectRatio(100, 100);
            cropImageView.setCropShape(CropImageView.CropShape.OVAL);
            cropImageView.setGuidelines(CropImageView.Guidelines.OFF);
            cropImageView.setImageUriAsync(imageUri);
            cropImageView.setScaleType(CropImageView.ScaleType.FIT_CENTER);

            mDialogPlus = DialogPlus.newDialog(AddContactActivity.this)
                    .setHeader(headerView)
                    .setContentHolder(new ViewHolder(mainContainer))
                    .setInAnimation(android.R.anim.slide_in_left)
                    .setOutAnimation(android.R.anim.slide_out_right)
                    .setGravity(Gravity.CENTER_VERTICAL)
                    .setCancelable(true)
                    .create();
            mDialogPlus.show();


            View.OnClickListener profilePickerCloseBanListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int viewId = v.getId();
                    if (viewId == R.id.header_close_btn || viewId == R.id.cancel_crop) {
                        if (mDialogPlus != null && mDialogPlus.isShowing()) {
                            mDialogPlus.dismiss();
                        }
                    }else if(viewId == R.id.crop_and_save){
//                        showLoadingIndicator(true); TODO
                        Bitmap cropped =  cropImageView.getCroppedImage(104, 104);
                        if (cropped != null){
                            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                            fixMediaDir();
                            Uri tempUri = getImageUri(AddContactActivity.this.getApplicationContext(), cropped);

                            // CALL THIS METHOD TO GET THE ACTUAL PATH
                            filePath = getRealPathFromURI(tempUri);
                            finalFile = new File(getRealPathFromURI(tempUri));

                            if(!TextUtils.isEmpty(filePath) || finalFile != null) {
                                Log.d(TAG, finalFile.getAbsolutePath());
                                Glide.with(AddContactActivity.this).load(finalFile).diskCacheStrategy(DiskCacheStrategy.ALL).into(profile_pic);
                            }

                        }
                        if (mDialogPlus != null && mDialogPlus.isShowing()) {
                            mDialogPlus.dismiss();
                        }
                    }
                }
            };
            headerCloseBtn.setOnClickListener(profilePickerCloseBanListener);
            cancleBtn.setOnClickListener(profilePickerCloseBanListener);
            submitBtn.setOnClickListener(profilePickerCloseBanListener);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        ContentResolver contentResolver = inContext.getContentResolver();
        String dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
        String path = MediaStore.Images.Media.insertImage(contentResolver, inImage, "Avatar" + dateFormat, "Profile Image");
        if(!TextUtils.isEmpty(path)) {
            return Uri.parse(path);
        }else {
            return null;
        }
    }

    public String getRealPathFromURI(Uri uri) {
        if(uri != null) {
            Cursor cursor = AddContactActivity.this.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }else {
            return null;
        }
    }

    void fixMediaDir() {
        File sdcard = Environment.getExternalStorageDirectory();
        if (sdcard != null) {
            File mediaDir = new File("/sdcard/Pictures");
            if (!mediaDir.exists()) {
                mediaDir.mkdirs();
            }
        }
    }

    private void askForReadWritePermission() {
        if (ContextCompat.checkSelfPermission(AddContactActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(AddContactActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(AddContactActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_WRITE);

        }else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    private void setLoadingIndicator() {
        if(findViewById(R.id.avloadingIndicatorView) !=null) {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);
        }
    }

    private void hideLoadingIndicator() {
        if(findViewById(R.id.avloadingIndicatorView) !=null) {
            findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_WRITE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(getPickImageChooserIntent(), 200);
                }
                return;
            }
        }
    }
}
