package contact.contactmanagement;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import contact.contactmanagement.DatabaseUtils.Contacts;
import contact.contactmanagement.Utils.CustomListItemInterface;
import contact.contactmanagement.Utils.TextUtils;
import de.hdodenhof.circleimageview.CircleImageView;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder>{

    private static final String TAG = ContactAdapter.class.getSimpleName();
    private ArrayList<Contacts> mContactList;
    private Context mContext;
    private CustomListItemInterface onClickListener;
    String prevInitial = "AA";
    String currInitial;
    int prevPos;
    public ContactAdapter(ArrayList<Contacts> dataList,Context context,CustomListItemInterface onClickListener) {
        this.mContactList = dataList;
        this.mContext = context;
        this.onClickListener = onClickListener;
    }

    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ContactAdapter.ViewHolder holder, int position) {
            Contacts currentContactInfo = mContactList.get(position);
        if(currentContactInfo != null){
            holder.populateContactInfo(currentContactInfo,position);
        }

    }


    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mFavouriteImg;
        ImageView mProfileIv;
        TextView mFirstName;
        TextView mLastName;
        Contacts currentContactData;
        RelativeLayout name_initial_container;
        TextView name_initial;
        ImageView favImg;
        int currentPosition;
        public ViewHolder(View itemView) {
            super(itemView);
            mFavouriteImg = (ImageView) itemView.findViewById(R.id.fav_icon);
            mProfileIv = (ImageView) itemView.findViewById(R.id.profile_image);
            mFirstName = (TextView) itemView.findViewById(R.id.first_name_text_view);
            mLastName = (TextView) itemView.findViewById(R.id.last_name_text_view);
            name_initial_container = (RelativeLayout) itemView.findViewById(R.id.fav_container);

            name_initial = (TextView) itemView.findViewById(R.id.name_inital);
            favImg = (ImageView) itemView.findViewById(R.id.fav_icon);
            favImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onListItemClickListener(v, currentContactData);
                }
            });
        }

        public void populateContactInfo(Contacts data,int position){
            currentContactData = data;
            currentPosition = position;
            setCurrentInitialValue(data);
            if(position >=  prevPos){
                if(position == 0 && isFavourite(data)){
                    showFavouriteView();
                }else if(!isFavourite(data)){
                    if(!currInitial.equalsIgnoreCase(prevInitial)){
                        showInitialView(currInitial);
                        setPreviousFlag(currInitial);
                    }else{
                        hideInitialView();
                    }
                }else{
                    hideInitialView();
                }
            }else{
                if(position == 0 && isFavourite(data)){
                    showFavouriteView();
                }else if (!isFavourite(data)){
                    if(position == 0) {
                        showInitialView(currInitial);
                    }else{
                        String aboveInitialValue = mContactList.get(position - 1).getFirstName();
                        if (!TextUtils.isEmpty(aboveInitialValue)) {
                            String initial = String.valueOf(aboveInitialValue.charAt(0));
                            if(!currInitial.equalsIgnoreCase(initial)){
                                showInitialView(currInitial);
                            }else{
                                hideInitialView();
                            }
                        }else{
                            hideInitialView();
                        }
                    }
                }else{
                    hideInitialView();
                }
            }
            prevPos = position;
            setProfilePic(data);
            setFirstName(data);
            setLastName(data);
        }

        private void hideInitialView() {
            name_initial_container.setVisibility(View.INVISIBLE);
            name_initial.setVisibility(View.INVISIBLE);

            name_initial.setText("");
            favImg.setVisibility(View.INVISIBLE);
        }

        private void showInitialView(String currInitial) {
            name_initial_container.setVisibility(View.VISIBLE);
            name_initial.setVisibility(View.VISIBLE);
            String tempString = currInitial.toUpperCase();
            name_initial.setText(tempString);
            favImg.setVisibility(View.INVISIBLE);
        }

        private void showFavouriteView() {
            name_initial_container.setVisibility(View.VISIBLE);
            name_initial.setVisibility(View.INVISIBLE);
            favImg.setVisibility(View.VISIBLE);
            name_initial.setText("");
        }

        private boolean isFavourite(Contacts data) {
            boolean isCurrentContactIsFavourite = false;
            String isCurrentContactIsFavouriteStr = data.getIsFavourite();
            if(!TextUtils.isEmpty(isCurrentContactIsFavouriteStr) && isCurrentContactIsFavouriteStr.equalsIgnoreCase("1")){
             isCurrentContactIsFavourite = true;
            }
            return  isCurrentContactIsFavourite;
        }

        private void setIsFavourite(Contacts data){
            String isFavouriteContact = data.getIsFavourite();
            if(!TextUtils.isEmpty(isFavouriteContact) && isFavouriteContact.equalsIgnoreCase("1")){
                mFavouriteImg.setVisibility(View.VISIBLE);
            }else{
                mFavouriteImg.setVisibility(View.INVISIBLE);
            }
        }

        private void setProfilePic(Contacts data){
             String imageUrl = data.getProfile_url();
            if(!TextUtils.isEmpty(imageUrl)){
                Glide.with(mContext).load(imageUrl).placeholder(R.drawable.missing_image).dontAnimate().dontTransform().into(mProfileIv);
            }else{
                Log.d(TAG, "image url not found");
            }
        }

        private  void setFirstName(Contacts data){
            String firstName = (String) data.getFirstName();
            if(!TextUtils.isEmpty(firstName)){
                mFirstName.setText(firstName);
            }else{
                Log.d(TAG, "first name not found");
            }
        }

        private void setCurrentInitialValue(Contacts data){
            String firstName = (String) data.getFirstName();
            if(!TextUtils.isEmpty(firstName)){
                currInitial = String.valueOf(firstName.charAt(0));
            }
        }

        private void setPreviousFlag(String flagValue){
            prevInitial = flagValue;
        }

        private  void setLastName(Contacts data){
            String lastname = (String) data.getLastName();
            if(!TextUtils.isEmpty(lastname)){
                mLastName.setText(lastname);
            }else{
                Log.d(TAG, "last name not found");
            }
        }

    }
}
