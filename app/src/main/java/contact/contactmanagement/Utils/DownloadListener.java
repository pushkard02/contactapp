package contact.contactmanagement.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public interface DownloadListener {

    public void onDataDownloaded(ArrayList<String> dataList,HashMap<String,String> dataHashmap,Object dataObject);
}
