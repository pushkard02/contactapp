package contact.contactmanagement.Utils;

/**
 * Created by pushkar on 30/7/16.
 */
public interface ContactDBConstants {
    String IS_FAVOURITE_CONTACT = "favorite";
    String CONTACT_PROFILE_PIC = "profile_pic";
    String CONTACT_FIRST_NAME = "first_name";
    String CONTACT_LAST_NAME = "last_name";
    String CONTACT_MOBILE_NUMBER = "phone_number";
    String CONTACT_EMAIL_ID = "email";
    String CONTACT_ID = "id";
    String CONTACT_URL = "url";
    String CONTACT_LOCAL_PIC_URL = "local_pic_url";
}
