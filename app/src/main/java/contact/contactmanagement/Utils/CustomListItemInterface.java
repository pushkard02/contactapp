package contact.contactmanagement.Utils;

import android.view.View;

import contact.contactmanagement.DatabaseUtils.Contacts;

/**
 * Created by hitesh on 1/8/16.
 */
public interface CustomListItemInterface {
    void onListItemClickListener(View v,Contacts object);
}
