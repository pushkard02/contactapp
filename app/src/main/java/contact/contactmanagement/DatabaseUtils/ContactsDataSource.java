package contact.contactmanagement.DatabaseUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import contact.contactmanagement.Utils.ContactDBConstants;

public class ContactsDataSource {

    private static SQLiteDatabase database;
    private static DatabaseHelper dbHelper;
    private static String[] allColumns = { DatabaseHelper.COLUMN_CONTACT_ID, DatabaseHelper.COLUMN_FIRST_NAME, DatabaseHelper.COLUMN_LAST_NAME,
    DatabaseHelper.COLUMN_FAVOURITE,DatabaseHelper.COLUMN_MOBILE,DatabaseHelper.COLUMN_EMAIL,DatabaseHelper.COLUMN_PROFILE_URL,DatabaseHelper.COLUMN_CONTACT_URL,DatabaseHelper.COLUMN_LOCAL_PIC_URL};

    public ContactsDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public static void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public static void close() {
        dbHelper.close();
    }

    public boolean createContact(Contacts contacts) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.COLUMN_CONTACT_ID,contacts.getContactId());
            values.put(DatabaseHelper.COLUMN_FIRST_NAME, contacts.getFirstName());
            values.put(DatabaseHelper.COLUMN_LAST_NAME,contacts.getLastName());
            values.put(DatabaseHelper.COLUMN_MOBILE,contacts.getMobileNo());
            values.put(DatabaseHelper.COLUMN_EMAIL,contacts.getEmail());
            values.put(DatabaseHelper.COLUMN_PROFILE_URL, contacts.getProfile_url());
            values.put(DatabaseHelper.COLUMN_FAVOURITE, contacts.getIsFavourite());
            values.put(DatabaseHelper.COLUMN_CONTACT_URL, contacts.getContact_url());
            values.put(DatabaseHelper.COLUMN_LOCAL_PIC_URL,contacts.getLocalPicURL());
            long insertId = database.insertWithOnConflict(DatabaseHelper.TABLE_CONTACTS, BaseColumns._ID,
                    values,SQLiteDatabase.CONFLICT_REPLACE);
            close();
            return insertId==-1?false:true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    public List<Contacts> getAllContacts() {
        try {
            open();
            List<Contacts> contactsList = new ArrayList<Contacts>();

            Cursor cursor = database.query(DatabaseHelper.TABLE_CONTACTS,
                    allColumns,DatabaseHelper.COLUMN_FAVOURITE+"=?",new String[]{"1"}, null, null, "LOWER("+DatabaseHelper.COLUMN_FIRST_NAME+")");

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Contacts contacts = new Contacts(cursor);
                contactsList.add(contacts);
                cursor.moveToNext();
            }
            cursor.close();

            Cursor cursor_all = database.query(DatabaseHelper.TABLE_CONTACTS,
                    allColumns,DatabaseHelper.COLUMN_FAVOURITE+"=?",new String[]{"0"}, null, null, "LOWER("+DatabaseHelper.COLUMN_FIRST_NAME+")");

            cursor_all.moveToFirst();
            while (!cursor_all.isAfterLast()) {
                Contacts contacts = new Contacts(cursor_all);
                contactsList.add(contacts);
                cursor_all.moveToNext();
            }
            cursor_all.close();


            close();
            return contactsList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Contacts> getContactListFromDb(Context context) {
        ContactsDataSource contactsDataSource = new ContactsDataSource(context);
        ArrayList arrayList = new ArrayList<>(contactsDataSource.getAllContacts());
        return arrayList;
    }

    public static void SaveContactList(ArrayList<HashMap<String, Object>> contactList, Context context) {
        for (HashMap<String,Object> contactHashmap:contactList) {
            Contacts contacts = new Contacts(contactHashmap);
            ContactsDataSource contactsDataSource = new ContactsDataSource(context);
            contactsDataSource.createContact(contacts);
        }
    }

    public static void SaveContactList(HashMap<String, Object> contactList, Context context) {

        if(contactList != null) {
            Contacts contacts = new Contacts(contactList);
            ContactsDataSource contactsDataSource = new ContactsDataSource(context);
            contactsDataSource.createContact(contacts);
        }
    }

    public static Contacts getContact(Context context,long contactId){
        Contacts contacts = new Contacts();
        try {
            open();
            List<Contacts> contactsList = new ArrayList<Contacts>();

            Cursor cursor = database.query(DatabaseHelper.TABLE_CONTACTS,
                    allColumns, ContactDBConstants.CONTACT_ID+"=?", new String[]{contactId+""}, null, null, null);
//            Cursor findEntry = db.query("sku_table", columns, "owner=?", new String[] { owner }, null, null, null);
            if(cursor!=null && cursor.moveToFirst()) {
                contacts = new Contacts(cursor);
            }

            cursor.close();
            close();
            return contacts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  contacts;
    }
}

