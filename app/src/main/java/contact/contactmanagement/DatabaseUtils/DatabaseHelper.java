package contact.contactmanagement.DatabaseUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import contact.contactmanagement.Utils.ContactDBConstants;


public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "contacts.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_CONTACTS = "contacts";
    public static final String COLUMN_CONTACT_ID=ContactDBConstants.CONTACT_ID;
    public static final String COLUMN_FAVOURITE=ContactDBConstants.IS_FAVOURITE_CONTACT;
    public static final String COLUMN_FIRST_NAME=ContactDBConstants.CONTACT_FIRST_NAME;
    public static final String COLUMN_LAST_NAME=ContactDBConstants.CONTACT_LAST_NAME;
    public static final String COLUMN_MOBILE=ContactDBConstants.CONTACT_MOBILE_NUMBER;
    public static final String COLUMN_EMAIL=ContactDBConstants.CONTACT_EMAIL_ID;
    public static final String COLUMN_PROFILE_URL= ContactDBConstants.CONTACT_PROFILE_PIC;
    public static final String COLUMN_CONTACT_URL= ContactDBConstants.CONTACT_URL;
    public static final String COLUMN_LOCAL_PIC_URL= ContactDBConstants.CONTACT_LOCAL_PIC_URL;


    //Query to create database
    private static final String CREATE_DATABASE="create table "+TABLE_CONTACTS +"( "+COLUMN_CONTACT_ID+
            " integer primary key autoincrement, " +COLUMN_FAVOURITE+" text, "+COLUMN_FIRST_NAME+" text, "+COLUMN_LAST_NAME+" text, "+
            COLUMN_MOBILE+" text, "+COLUMN_EMAIL+" text, "+COLUMN_PROFILE_URL+" text, "+COLUMN_CONTACT_URL+" text, "+COLUMN_LOCAL_PIC_URL+" text);";

    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
