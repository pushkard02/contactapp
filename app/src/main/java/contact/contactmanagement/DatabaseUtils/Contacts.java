package contact.contactmanagement.DatabaseUtils;

import android.database.Cursor;

import java.io.Serializable;
import java.util.HashMap;

import contact.contactmanagement.Utils.ContactDBConstants;

public class Contacts implements Serializable{

    private long contact_id;
    private String first_name;
    private String last_name;
    private String mobile_no;
    private String email;
    private String is_favourite;
    private String profile_url;
    private String contact_url;
    private String localPicURL;

    public Contacts() {

    }
    public Contacts(HashMap<String,Object> dataHashmap) {
        if(dataHashmap!=null) {
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_FIRST_NAME)) {
                this.first_name = (String) dataHashmap.get(ContactDBConstants.CONTACT_FIRST_NAME);
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_ID)) {
                this.contact_id = Long.valueOf(dataHashmap.get(ContactDBConstants.CONTACT_ID)+"");
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_LAST_NAME)) {
                this.last_name = (String) dataHashmap.get(ContactDBConstants.CONTACT_LAST_NAME);
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_MOBILE_NUMBER)) {
                this.mobile_no = (String) dataHashmap.get(ContactDBConstants.CONTACT_MOBILE_NUMBER);
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_EMAIL_ID)) {
                this.email = (String) dataHashmap.get(ContactDBConstants.CONTACT_EMAIL_ID);
            }
            if(dataHashmap.containsKey(ContactDBConstants.IS_FAVOURITE_CONTACT)) {
                String value = "0";
                if(dataHashmap.get(ContactDBConstants.IS_FAVOURITE_CONTACT) != null && !dataHashmap.get(ContactDBConstants.IS_FAVOURITE_CONTACT).equals(null)) {
                    Boolean favouriteValue = (boolean) dataHashmap.get(ContactDBConstants.IS_FAVOURITE_CONTACT);
                    if (favouriteValue != null && favouriteValue) {
                        value = "1";
                    }
                }
                this.is_favourite = value;
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_PROFILE_PIC)) {
                this.profile_url = (String) dataHashmap.get(ContactDBConstants.CONTACT_PROFILE_PIC);
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_URL)) {
                this.contact_url = (String) dataHashmap.get(ContactDBConstants.CONTACT_URL);
            }
            if(dataHashmap.containsKey(ContactDBConstants.CONTACT_LOCAL_PIC_URL)) {
                this.localPicURL = (String) dataHashmap.get(ContactDBConstants.CONTACT_LOCAL_PIC_URL);
            }

        }
    }

    public Contacts(Cursor cursor) {
        if(cursor!=null) {
            this.first_name =  cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_FIRST_NAME));
            if(cursor.getColumnIndex(ContactDBConstants.CONTACT_ID) != -1) {
                this.contact_id = (long) cursor.getInt(cursor.getColumnIndex(ContactDBConstants.CONTACT_ID));
            }
            this.last_name =  cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_LAST_NAME));
            this.mobile_no =  cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_MOBILE_NUMBER));
            this.email =  cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_EMAIL_ID));
            if(cursor.getColumnIndex(ContactDBConstants.IS_FAVOURITE_CONTACT) != -1) {
                this.is_favourite = cursor.getString(cursor.getColumnIndex(ContactDBConstants.IS_FAVOURITE_CONTACT));
            }
            this.profile_url = cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_PROFILE_PIC));
            this.contact_url =  cursor.getString(cursor.getColumnIndex(ContactDBConstants.CONTACT_URL));
        }
    }

    public HashMap<String,Object> getHashmap(Contacts contact) {
        HashMap<String,Object> contactMap = new HashMap<>();
        contactMap.put(ContactDBConstants.CONTACT_ID,contact.getContactId());
        contactMap.put(ContactDBConstants.CONTACT_FIRST_NAME,contact.getFirstName());
        contactMap.put(ContactDBConstants.CONTACT_LAST_NAME,contact.getLastName());
        contactMap.put(ContactDBConstants.CONTACT_MOBILE_NUMBER,contact.getMobileNo());
        contactMap.put(ContactDBConstants.CONTACT_EMAIL_ID, contact.getEmail());
        contactMap.put(ContactDBConstants.CONTACT_PROFILE_PIC, contact.getProfile_url());
        contactMap.put(ContactDBConstants.IS_FAVOURITE_CONTACT,contact.getIsFavourite());
        contactMap.put(ContactDBConstants.CONTACT_URL,contact.getContact_url());
        contactMap.put(ContactDBConstants.CONTACT_LOCAL_PIC_URL,contact.getLocalPicURL());

        return contactMap;
    }

    public long getContactId(){
        return contact_id;
    }

    public String getFirstName(){
        return first_name;
    }

    public String getLastName(){
        return last_name;
    }

    public String getMobileNo(){
        return mobile_no;
    }

    public String getEmail(){
        return email;
    }

    public String getIsFavourite(){
        return is_favourite;
    }

    public void setContact_id(long id){
        this.contact_id = id;
    }

    public void setFirstName(String name) {
        this.first_name = name;
    }

    public void setLastName(String name) {
        this.last_name = name;
    }

    public void setMobileNo(String mobile) {
        this.mobile_no = mobile;
    }

    public void setEmail(String emailId) {
        this.email = emailId;
    }

    public void setIsFavourite(String favourite) {
        this.is_favourite = favourite;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }


    public String getContact_url() {
        return contact_url;
    }

    public void setContact_url(String contact_url) {
        this.contact_url = contact_url;
    }

    public String getLocalPicURL() {
        return localPicURL;
    }
    public void setLocalPicURL(String local_pic_url) {
        this.localPicURL = local_pic_url;
    }


}
